;
; APCKiosk Version: 	0.2
; Language:       		AutoHotkey
; Platform:       		Windows7/8.1
; Author:         		Andre Louw <alouw@apcglobal.com>
;
; Script Function:
;	- This script starts by requesting two URL's to show
; 	- Once URL's are specified it creates a ,cfg file that stores the URL Details
;	- Next the script gets monitor information (How Many and the dimensions of each)
;	- Next, depending on the number of the screens, the script will create a GUI for each screen and
;		embeds an Explorer shell in each GUI. The URL's specified at the beginning of the script will determine
;			what pages are opened in the GUI's. 
;	- Each GUI will be set to Always-on-top and full screen with no border or menu (Kiosk Mode). 
; 	- Windows buttons are disabled.
; 	- The web apges are refreshed every 10 mintues. 
;  	- The short cut to change the URL's are Windows+n, this request for the URL's again. 
; 	-	  


;################ AutoHotkey settings ################
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

ConfigFile = %A_ScriptDir%\apckiosk.cfg

;################ URL's ################
IfExist %ConfigFile%
	{
		Gosub URLParse
		Gosub DefineMonitors
		Gosub Main
	}
else
	Gosub URLInput
;msgbox URL 1 : %URL1% `nURL 2 : %URL2% 




;################ Get Monitor Information ################
DefineMonitors:
SysGet, MonitorCount, MonitorCount

; --- For Every Monitor get and assign Work Area variables 
Loop, %MonitorCount%
{
   SysGet, MonitorName, MonitorName, %A_Index%
    SysGet, Monitor, Monitor, %A_Index%
    SysGet, MonitorWorkArea, MonitorWorkArea, %A_Index%
    Mon%A_Index%Left = %MonitorWorkAreaLeft%
    Mon%A_Index%Top = %MonitorWorkAreaTop%
    Mon%A_Index%Right = %MonitorWorkAreaRight%
    Mon%A_Index%Bottom = %MonitorWorkAreaBottom%	
}
return



;################ Main Script (Open Kiosk Windows)################
Main:
;--- If there are 2 screens open 2 GUI's
if MonitorCount = 2

{
	;/* --- Calculate Screen Sizes ---*/
	
	Mon1Width := Mon1Right - Mon1Left
	Mon1Height := Mon1Bottom - Mon1Top
	Mon2Width := Mon2Right - Mon2Left
	Mon2Height := Mon2Bottom - Mon2Top
	
	;/* --- GUI Number 1 ---*/
	
	;Gui, 1: 	+HwndhWindow
	Gui, 1: Add, ActiveX, y0 x0 w%Mon1Width% h%Mon1Height% vWB1, Shell.Explorer		;Specify GUI inside Width and Height and enable Shell explorer 
	Gui, 1: +AlwaysOnTop -Border -SysMenu +Owner -Caption -ToolWindow 				;Enable always-on-top disable border and menus
	Gui, 1: show, x%Mon1Left% y%Mon1Top%, APCKiosk1	, 				 				;Specify width, height and name of GUI Window
	WinSet, Style, -0xC00000, A														;Remove Style and remaining border
	WinSet, Style, -0x40000, A
	;GUI_Handle=ahk_id %hWindow%													;Specify handle
	WB1.Navigate(Screen1URL)															;Specify URL to show											
	;fs:=0
	
	;/* --- GUI Number 2 ---*/
	
	;Gui 2: +HwndhWindow
	Gui, 2: Add, ActiveX, y0 x0 w%Mon2Width% h%Mon2Height% vWB2, Shell.Explorer		;Specify GUI inside Width and Height and enable Shell explorer 	
	Gui, 2: +AlwaysOnTop -Border -SysMenu +Owner -Caption -ToolWindow 				;Enable always-on-top disable border and menus
	Gui, 2: show, x%Mon2Left% y%Mon2Top%, APCKiosk2	,								;Specify width, height and name of GUI Window
	
	WinSet, Style, -0xC00000, A														;Remove Style and remaining border
	WinSet, Style, -0x40000, A
	;GUI_Handle=ahk_id %hWindow%													;Specify handle							
	WB2.Navigate(Screen2URL)															;Specify URL to show		
	;fs:=0
	
	#Persistent
	SetTimer, RefreshPages, 600000
	return
}

else
{
	;/* --- Calculate Screen Size ---*/
	
	Mon1Width := Mon1Right - Mon1Left
	Mon1Height := Mon1Bottom - Mon1Top
	
	;/* --- Only 1 GUI ---*/
	
	;Gui 1: +HwndhWindow															
	Gui, 1: Add, ActiveX, y0 x0 w%Mon1Width% h%Mon1Height% vWB1, Shell.Explorer		;Specify GUI inside Width and Height and enable Shell explorer 
	Gui, 1: +AlwaysOnTop -Border -SysMenu +Owner -Caption -ToolWindow 				;Enable always-on-top disable border and menus
	Gui, 1: show, x%Mon1Left% y%Mon1Top%, APCKiosk, 								;Specify width, height and name of GUI Window
	WinSet, Style, -0xC00000, A														;Remove Style and remaining border		
	WinSet, Style, -0x40000, A		
	;GUI_Handle=ahk_id %hWindow%													;Specify handle	
	WB1.Navigate(Screen1URL)															;Specify URL to show
	;fs:=0
	
	#Persistent
	SetTimer, RefreshPages, 600000
	return
}
return

RefreshPages:
	WB1.Refresh
	WB2.Refresh
	return

URLInput:
	Gui Destroy
	FileDelete %ConfigFile% 
	
	inputbox,  text, URL 1,,,500,100
    fileappend, URL 1 - %text%`n, apckiosk.cfg
    inputbox, text, URL 2,,,500,100
    fileappend, URL 2 - %text%`n, apckiosk.cfg
	
	Gosub URLParse
	
	msgbox URL 1 : %Screen1URL% `nURL 2 : %Screen2URL% 
	
	Gosub DefineMonitors
	Gosub Main

URLParse:
	LinkCount = 1
	Loop, read, %ConfigFile%, %DestFile%
	{
		URLSearchString = %A_LoopReadLine%
		Gosub, URLSearch
	}
	
	Screen1URL := URL1
	Screen2URL := URL2
	return
	
	
	
	
URLSearch:
	StringGetPos, URLStart1, URLSearchString, http://
	StringGetPos, URLStart2, URLSearchString, https://

	;Find the left-most starting position:
	URLStart = %URLStart1%  ; Set starting default.
	Loop
	{
		ArrayElement := URLStart%A_Index%
		if ArrayElement =  ; End of the pseudo-array has been reached.
			break
		if ArrayElement = -1  ; This element is disqualified.
			continue
		if URLStart = -1
			URLStart = %ArrayElement%
		else ; URLStart has a valid position in it, so compare it with ArrayElement.
		{
			if ArrayElement <> -1
				if ArrayElement < %URLStart%
					URLStart = %ArrayElement%
		}
	}

	if URLStart = -1  
	return

	StringTrimLeft, URL, URLSearchString, %URLStart%  	
	Loop, parse, URL, %A_Tab%%A_Space%<>  				
	{
		URL = %A_LoopField%
		break  											
	}
	
	StringReplace, URLCleansed, URL, ",, All
	URL%LinkCount% = %URLCleansed%
	;msgbox URL assigned
	LinkCount += 1

	StringLen, CharactersToOmit, URL
	CharactersToOmit += %URLStart%
	StringTrimLeft, URLSearchString, URLSearchString, %CharactersToOmit%
	Gosub, URLSearch  
	return


	


;################Action buttons ################

#q:: Exitapp	
;#a:: Gui Destroy
!F4:: Exitapp
#r:: WB1.Refresh
#e:: WB2.Refresh
#n:: Gosub URLInput
																	;Press WindowsKey+q to quit script 
;################ Disabled Inputs ################
~LWin Up:: return													;Left WindowsKey disabled 
~RWin Up:: return													;Right WindowsKey disabled 