# README #



### What is this repository for? ###

This script startsup browsers in kiosk mode.
It detects the number of screens and will startup the number of kiosks required. 
* Version 0.2


### How do I get set up? ###

* Install AutoHotKey to be able to edit ahk script. 
* Configuration: 
* Dependencies: Windows 7/8.1, AutoHotKey.exe, IE11
* Deployment instructions: When running script for the first time specify URL's when prompted. Needs to be deployed with APCKioskWD.exe - A Watch dog app to monitor the Kiosk app

### Contribution guidelines ###

Get in touch with Owner

### Who do I talk to? ###

Andre Louw <alouw@apcglobal.com>